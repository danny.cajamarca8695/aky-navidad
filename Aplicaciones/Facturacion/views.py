from django.shortcuts import render,redirect
from .models import Cliente
from django.contrib import messages


def tu_sazon(request):
    # Lógica de la vista
    return render(request, 'tu_sazon.html')

def tu_comunidad(request):
    # Lógica de la vista
    return render(request, 'tu_comunidad.html')
def club(request):
    # Lógica de la vista
    return render(request, 'club.html')
def producto_aki(request):
    # Lógica de la vista
    return render(request, 'producto_aki.html')
def la_original(request):
    # Lógica de la vista
    return render(request, 'la_original.html')
def tres(request):
    # Lógica de la vista
    return render(request, '3x2.html')
def cuatro(request):
    # Lógica de la vista
    return render(request, '4x3.html')

def ofertas(request):
    # Lógica de la vista
    return render(request, 'ofertas.html')

def quincenazo(request):
    # Lógica de la vista
    return render(request, 'quincenazo.html')
