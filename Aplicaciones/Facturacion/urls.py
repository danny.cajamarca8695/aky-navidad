from django.urls import path
from . import views

urlpatterns=[
    path('',views.tu_sazon),
    path('tu_comunidad/',views.tu_comunidad, name='tu_comunidad'),
    path('club/',views.club, name='club'),
    path('producto_aki/',views.producto_aki, name='producto_aki'),
    path('la_original/',views.la_original, name='la_original'),
    path('3x2/',views.tres, name='3x2'),
    path('4x3/',views.cuatro, name='4x3'),
    path('ofertas/',views.ofertas, name='ofertas'),
    path('quincenazo/',views.quincenazo, name='quincenazo'),

]
